import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QHBoxLayout,QFileDialog
from PyQt5.QtCore import QCoreApplication
import parserVkLike as pLike
import programm
class Autorization(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.resize(312, 176)
        self.gridLayout = QtWidgets.QGridLayout(self)

        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 2)
        self.gridLayout.addLayout(self.verticalLayout_2, 1, 1, 1, 1)
        self.gridLayout.addLayout(self.verticalLayout_3, 1, 0, 1, 1)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.gridLayout.addLayout(self.horizontalLayout, 2, 1, 1, 1)

        self.label = QtWidgets.QLabel(self)
        self.label.setText("<html><head/><body><p align=\"center\"><span style=\" color:#ff0000;\">ДЛЯ РАБОТЫ ПРИЛОЖЕНИЯ ВАМ НЕОБХОДИМО </span></p><p align=\"center\"><span style=\" color:#ff0000;\">ВОЙТИ В СВОЙ АККАУНТ ВКОНТАКТЕ</span></p></body></html>")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self)
        self.label_2.setText("<html><head/><body><p align=\"right\">Login</p></body></html>")
        self.verticalLayout_3.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self)
        self.label_3.setText("<html><head/><body><p align=\"right\">Password</p></body></html>")
        self.verticalLayout_3.addWidget(self.label_3)


        self.login = QtWidgets.QLineEdit(self)
        self.verticalLayout_2.addWidget(self.login)
        self.password = QtWidgets.QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        self.verticalLayout_2.addWidget(self.password)

        self.pushButton = QtWidgets.QPushButton(self)
        self.pushButton.setText("Войти")
        self.horizontalLayout.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self)
        self.pushButton_2.setText("Закрыть")
        self.horizontalLayout.addWidget(self.pushButton_2)

        self.show()
        self.cliked()
    def cliked(self):
        self.pushButton_2.clicked.connect(QCoreApplication.instance().quit)
        self.pushButton.clicked.connect(self.auterezation)
    
    def auterezation(self):
        login=self.login.text()
        password=self.password.text()
        try:
            self.enter=(pLike.auterazation(login,password))
            self.win_2=Window_2(self.enter)
            self.win_2.show()
            self.close()
        except:

            self.label_4 = QtWidgets.QLabel(self)
            self.label_4.setText('Неправильный логин или пароль')
            self.verticalLayout_2.addWidget(self.label_4)

class Window_2(QWidget):

    def __init__(self,vkapi):
        super().__init__()
        self.vkapi=vkapi
        self.initUI_2()


    def initUI_2(self):
        self.resize(321, 226)
        self.gridLayout = QtWidgets.QGridLayout(self)

        self.horizontalLayout_2_1 = QtWidgets.QHBoxLayout()
        self.gridLayout.addLayout(self.horizontalLayout_2_1, 1, 0, 1, 1)
        self.verticalLayout_2_1 = QtWidgets.QVBoxLayout()
        self.gridLayout.addLayout(self.verticalLayout_2_1, 0, 0, 1, 1)
        self.horizontalLayout_2_2 = QtWidgets.QHBoxLayout()
        self.gridLayout.addLayout(self.horizontalLayout_2_2, 2, 0, 1, 1)
        self.horizontalLayout_2_2.setContentsMargins(131, 74, 0, 0)
        self.horizontalLayout_2_2.setSpacing(6)

        self.label_2_1 = QtWidgets.QLabel(self)
        self.label_2_1.setText("")
        self.horizontalLayout_2_1.addWidget(self.label_2_1)
        self.id = QtWidgets.QLabel(self)
        self.id.setText("Id      ")
        self.horizontalLayout_2_1.addWidget(self.id)
        self.label_2_2 = QtWidgets.QLabel(self)
        self.label_2_2.setText("<html><head/><body><p align=\"center\"><span style=\" font-size:10pt; color:#ff0000;\">Введите Id пользователя, которого вы </span></p><p align=\"center\"><span style=\" font-size:10pt; color:#ff0000;\">хотите проанализировать</span></p></body></html>")
        self.verticalLayout_2_1.addWidget(self.label_2_2)

        self.form_id = QtWidgets.QLineEdit(self)
        self.horizontalLayout_2_1.addWidget(self.form_id)

        self.cancle = QtWidgets.QPushButton(self)
        self.cancle.setText("Cancel")
        self.horizontalLayout_2_2.addWidget(self.cancle)
        self.start = QtWidgets.QPushButton(self)
        self.start.setText("Start")
        self.horizontalLayout_2_2.addWidget(self.start)

        self.show()
        self.clike()

    def clike(self):
        self.cancle.clicked.connect(QCoreApplication.instance().quit)
        self.start.clicked.connect(self.start_app)
    def start_app(self):
        self.vkapi_1=self.vkapi[0]
        self.vkapi_2=self.vkapi[1]
        self.id=self.form_id.text()
        self.result=programm.start_apps(self.id, self.vkapi_1, self.vkapi_2)
        self.requst_save=Request_Save(self.result)
        self.requst_save.show()
        self.close()

class Save_File(QWidget):
    def __init__(self,result):
        super().__init__()
        self.result = result
        self.initUI()

    def initUI(self):
        self.setGeometry(10,10,640,480)
        self.saveFileDialog()
        self.show()

    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "",
                                                  "All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
           with open(fileName,'w')as file:
               file.write(self.result)
class Request_Save(QWidget):
    def __init__(self,result):
        super().__init__()
        self.result = result
        self.initUI_3()

    def initUI_3(self):
        self.resize(361,250)
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.verticalLayout.addLayout(self.verticalLayout_2)

        self.label = QtWidgets.QLabel(self)
        self.horizontalLayout.addWidget(self.label)
        self.label.setText('{}'.format(self.result))


        self.pushButton_2 = QtWidgets.QPushButton(self)
        self.verticalLayout.addWidget(self.pushButton_2)
        self.pushButton = QtWidgets.QPushButton(self)
        self.verticalLayout.addWidget(self.pushButton)
        self.pushButton_2.setText("Выход")
        self.pushButton.setText("Сохранить")

        self.show()
        self.clike()

    def clike(self):
        self.pushButton_2.clicked.connect(QCoreApplication.instance().quit)
        self.pushButton.clicked.connect(self.R_yes)
    def R_yes(self):
        Save_File(self.result)
        self.close()
app = QApplication(sys.argv)
form=Autorization()
app.exec_()