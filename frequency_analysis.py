from collections import Counter
from _operator import itemgetter

def frequency_analis(doc):
    tf=Counter(doc)
    tf={term:round(tf_item/len(doc),6) for term, tf_item in tf.items()}
    tf=sorted(tf.items(),key=itemgetter(1),reverse=True)
    str_tf=''
    for i in range(len(tf)):
        fr=(tf[i][1])
        word=tf[i][0]
        if i<len(tf)-1:
            str_tf+='{} {}\n'.format(word,fr)
        else:
            str_tf += '{} {}'.format(word,fr)
    return str_tf.strip()

def start(file_name):
    with open('после обработки {}'.format(file_name),'r',encoding='utf-8') as f:
        file=f.read().split()
        with open('частотный анализ {}'.format(file_name),'w',encoding='utf-8') as f2:
            print(frequency_analis(file),file=f2)
