import codecs,re,string,pymorphy2
from nltk.corpus import stopwords


def process(text,stop_words,file_name):
    #Удаление слов
    #Нормализация регистра
    #Удаление цифр
    #Лемматизация
    
    lemmatizer=pymorphy2.MorphAnalyzer()
    
    text=re.findall(r'\w*',text.lower())
    text= [lemmatizer.parse(word)[0].normal_form 
           for word in text if lemmatizer.parse(word)[0].normal_form not in stop_words and not word.isdigit()]
    text= ' '.join(text).split()
    print(text)
    if len(text)>0:
        text=(' '.join(text))
        
        file=codecs.open(r'''после обработки {}'''.format(file_name),'a','utf-8')
        file.write('{} \n'.format(text))

def start(file_name):
    punct=list(string.punctuation)
    my_words=["это","свой","который","ваш","самый","другой","также","мой",'тебе','каждый',"весь","наш","еще","очень",'com','http','на','и']
    stopwords_list=stopwords.words('russian')+punct+my_words
    print(stopwords_list)
    file=codecs.open(r'''{}'''.format(file_name),'r','utf-8')
    file=file.read().split(' *******************************************************************')

    for line in (file):
        process(line,stopwords_list,file_name)
    print("End")