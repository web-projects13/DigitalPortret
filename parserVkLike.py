# -*- encoding=utf-8 -*-
import vk, time
import threading




def autorization(password, login, app_id):  # Авторизация
    session = vk.AuthSession(app_id=app_id, user_login='{}'.format(login), user_password='{}'.format(password))
    vkapi = vk.API(session)
    return vkapi


def get_users_groups(users_id, vkapi):  # получаем список всех групп пользователя
    list_id_group = vkapi.groups.get(user_id=users_id, v=5.80)
    return list_id_group


def passage_on_lists(vkapi,user_id, list_groups_id):
    for group_id in list_groups_id:
        get_posts_user_like(vkapi,user_id, group_id)


def get_posts_user_like(vkapi,user_id, group_id):
    posts = vkapi.wall.get(owner_id='-{}'.format(group_id), count=100, v=5.87)['items']
    with open('{}.txt'.format(user_id), 'a', encoding='utf-8') as f:
        for post in posts:
            time.sleep(0.3)
            like = \
            vkapi.likes.isLiked(user_id=user_id, owner_id='-{}'.format(group_id), type='post', item_id=post['id'],
                                v=5.87)['liked']
            if like == 1:
                try:

                    f.write(post['copy_history'][0]['text'])
                    f.write("\n *******************************************************************\n")
                except:
                    if post['text'] != '':
                        f.write(post['text'])
                        f.write("\n *******************************************************************\n")

def auterazation(login,password,app_id_1='6445166', app_id_2='6638457'):
    vkapi_1 = autorization(password, login, app_id_1)  # Авторизация
    vkapi_2 = autorization(password, login, app_id_2)  # Авторизация
    return [vkapi_1,vkapi_2]
def start(user_id_for_pars,vkapi_1,vkapi_2):

    list_id_groups = get_users_groups(user_id_for_pars, vkapi_2)['items']  # ID групп пользователя

    t1 = threading.Thread(target=passage_on_lists, args=(vkapi_1,user_id_for_pars, list_id_groups[:len(list_id_groups) // 2],))
    t2 = threading.Thread(target=passage_on_lists, args=(vkapi_2,user_id_for_pars, list_id_groups[len(list_id_groups) // 2:],))

    t1.start()
    t2.start()

    t1.join()
    t2.join()
