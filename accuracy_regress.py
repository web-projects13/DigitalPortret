import os,pickle
import numpy as np
import sklearn.linear_model as lm


def dowlond_data():
    frequency=[]
    with open('перечень_слов_для_регресс.txt', 'r') as file:
        words_list = ''.join(file.read().split('\n')).split(' ')
        words_user = []
        fr = []
        frequency = []
        with open('R:\Учеба\Диплом\Корреляционный анализ\частота_слов_каждый пользовательь.txt', 'r') as file2:
            file_frequency = file2.readlines()
            for word_frequency in file_frequency:
                word_frequency = ''.join(word_frequency.split('\n')).split('  ')
                words_user.append(word_frequency[0])
                fr.append(word_frequency[1])
            for word in words_list:
                if word in words_user:
                    freq = (fr[words_user.index(word)].split(','))
                    for i in range(len(freq)):
                        freq[i] = float(freq[i])
                    frequency.append(freq)
    result_tetst = []
    trait = 'самоконтроль'
    list_file_name = os.listdir('R:\Учеба\Диплом\Результаты тестирования\Первичные факторы')
    for name in list_file_name:
        with open('R:\Учеба\Диплом\Результаты тестирования\Первичные факторы\{}'.format(name), 'r') as f:
            file = f.read().split(' ')
            result_trait = file[file.index(trait) + 1]
            result_tetst.append(result_trait)
    for i in range(len(result_tetst)):
        result_tetst[i]=int(result_tetst[i])
    data=[result_tetst,frequency]
    return data
def model(data):
    result_tests = data[0][0:10]
    frequency = data[1]
    frequency=np.array(frequency).transpose()[0:10]
    file_model_Active = open('modelControl', 'rb')
    skm = pickle.load(file_model_Active)
    acc=int(skm.score(frequency, result_tests))
    return (acc)

data=dowlond_data()
print(model(data))