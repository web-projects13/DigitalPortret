#base_precedent двумерный массив [[прецедент1],[прецедент2],[прецедентN]]
#weight одномерный массив
#situation_now одномерный массив


import math
import numpy as np

def distance(precedent_i,situation_now,weight):
    d=0
    for i in range(len(precedent_i)):
        d=d+(weight[i]*(precedent_i[i]-situation_now[i])**2)
    d=round(math.sqrt(d),2)
    return d
def distance_max(base_precedent,weight):

    base_precedent=np.array(base_precedent)
    base_precedent=base_precedent.transpose()
    parametrs=[]
    d_max=[]
    for parametr in base_precedent:
        x_left=min(parametr)-2
        x_right=max(parametr)+2
        parametrs.append([x_left,x_right])
    for x in range(len(parametrs)):
        d_max.append(math.sqrt(weight[x]*(parametrs[x][0]-parametrs[x][1])**2))
    return (max(d_max))


def proximity(base_precedent,weight,situation_now):
    d_max=distance_max(base_precedent,weight)
    for precedent_i in base_precedent:
        d=distance(precedent_i,situation_now,weight)
        h=1-(d/d_max)
        return abs(h)


#Струкутура базы:активность,самоконтроль,общительность,напряженность, депрессивность, доверчивость, мечтательность,эмоц стабильность

