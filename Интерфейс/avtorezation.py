# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'авторизация.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget


class Ui_Aurorization(object):
    def setupUi(self, Aurorization):
        Aurorization.setObjectName("Aurorization")
        Aurorization.setEnabled(True)
        Aurorization.resize(312, 176)
        Aurorization.setMouseTracking(True)
        Aurorization.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        Aurorization.setAutoFillBackground(False)
        self.gridLayout = QtWidgets.QGridLayout(Aurorization)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_2 = QtWidgets.QLabel(Aurorization)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_3.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(Aurorization)
        self.label_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.label_3.setWordWrap(False)
        self.label_3.setOpenExternalLinks(False)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_3.addWidget(self.label_3)
        self.gridLayout.addLayout(self.verticalLayout_3, 1, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.login = QtWidgets.QLineEdit(Aurorization)
        self.login.setAutoFillBackground(False)
        self.login.setClearButtonEnabled(True)
        self.login.setObjectName("login")
        self.verticalLayout_2.addWidget(self.login)
        self.password = QtWidgets.QLineEdit(Aurorization)
        self.password.setTabletTracking(True)
        self.password.setObjectName("password")
        self.verticalLayout_2.addWidget(self.password)
        self.gridLayout.addLayout(self.verticalLayout_2, 1, 1, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Aurorization)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_2 = QtWidgets.QPushButton(Aurorization)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.pushButton = QtWidgets.QPushButton(Aurorization)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 1, 1, 1)
        self.retranslateUi(Aurorization)
        QtCore.QMetaObject.connectSlotsByName(Aurorization)
        self.show()

    def retranslateUi(self, Aurorization):
        _translate = QtCore.QCoreApplication.translate
        Aurorization.setWindowTitle(_translate("Aurorization", "Form"))
        self.label_2.setText(_translate("Aurorization", "<html><head/><body><p align=\"right\">Login</p></body></html>"))
        self.label_3.setText(_translate("Aurorization", "<html><head/><body><p align=\"right\">Password</p></body></html>"))
        self.label.setText(_translate("Aurorization", "<html><head/><body><p align=\"center\"><span style=\" color:#ff0000;\">ДЛЯ РАБОТЫ ПРИЛОЖЕНИЯ ВАМ НЕОБХОДИМО </span></p><p align=\"center\"><span style=\" color:#ff0000;\">ВОЙТИ В СВОЙ АККАУНТ ВКОНТАКТЕ</span></p></body></html>"))
        self.pushButton_2.setText(_translate("Aurorization", "Закрыть"))
        self.pushButton.setText(_translate("Aurorization", "Войти"))
        self.show()
        #self.pushButton.clicked.connect(self.on_click)


    """def on_click(self,Aurorization):
        
        print(self.login.text())"""

app = QApplication(sys.argv)
form=Ui_Aurorization()
app.exec_()