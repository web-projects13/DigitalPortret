# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ввод_id.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(321, 226)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout_2.setContentsMargins(131, 74, 0, 0)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.cancle = QtWidgets.QPushButton(Form)
        self.cancle.setObjectName("cancle")
        self.horizontalLayout_2.addWidget(self.cancle)
        self.start = QtWidgets.QPushButton(Form)
        self.start.setObjectName("start")
        self.horizontalLayout_2.addWidget(self.start)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setText("")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.id = QtWidgets.QLabel(Form)
        self.id.setObjectName("id")
        self.horizontalLayout.addWidget(self.id)
        self.form_id = QtWidgets.QLineEdit(Form)
        self.form_id.setObjectName("form_id")
        self.horizontalLayout.addWidget(self.form_id)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.cancle.setText(_translate("Form", "Cancel"))
        self.start.setText(_translate("Form", "Start"))
        self.id.setText(_translate("Form", "Id      "))
        self.label_2.setText(_translate("Form", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt; color:#ff0000;\">Введите Id пользователя, которого вы </span></p><p align=\"center\"><span style=\" font-size:10pt; color:#ff0000;\">хотите проанализировать</span></p></body></html>"))

