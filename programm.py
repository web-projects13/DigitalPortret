import parserVkLike as pLike
import  parserVK_wall as pWall
import preparation_text as tx
import  frequency_analysis as frq
import precedent as pr
import pickle
import numpy as np


#Создания файла для сохранения данных


def start_apps(id_user_for_pars,vkapi_1,vkapi_2):
    with open('{}.txt'.format(id_user_for_pars), 'tw', encoding='utf-8') as f:
        pass


    pWall.start(vkapi_1, id_user_for_pars)
    #Парсинг лайков пользователя

    pLike.start(id_user_for_pars,vkapi_1,vkapi_2)



    # Обработка записей(удаления стоп слов,нормализация регистра,лимитизация)

    tx.start('{}.txt'.format(id_user_for_pars))

    #Частотный анализ

    frq.start('{}.txt'.format(id_user_for_pars))

    #Регрессионный анализ

    file_model_Active=open('modelActive','rb')
    file_model_Control=open('modelControl','rb')
    file_model_Depress=open('modelDepress','rb')
    file_model_Dominir=open('modelDominir','rb')
    file_model_Dover=open('modelDover','rb')
    file_model_Dream=open('modelDream','rb')
    file_model_Labil=open('modelLabil','rb')
    file_model_Napr=open('modelNapr','rb')
    file_model_Nastoy=open('modelNastoy','rb')
    file_model_Obshe=open('modelObshe','rb')
    file_model_Predosmotr=open('modelPredosmotr','rb')


    model_Active=pickle.load(file_model_Active)
    model_Control=pickle.load(file_model_Control)
    model_Depress=pickle.load(file_model_Depress)
    model_Dominir=pickle.load(file_model_Dominir)
    model_Dover=pickle.load(file_model_Dover)
    model_Dream=pickle.load(file_model_Dream)
    model_Labil=pickle.load(file_model_Labil)
    model_Napr=pickle.load(file_model_Napr)
    model_Nastoy=pickle.load(file_model_Nastoy)
    model_Obshe=pickle.load(file_model_Obshe)
    model_Predosmotr=pickle.load(file_model_Predosmotr)

    data=[[0.0]]#Переменная X

    with open('частотный анализ {}.txt'.format(id_user_for_pars),'r',encoding='utf-8') as f:
        word_fr=f.read().split('\n')# Частоты слов в записях пользователя
        list_word=[]
        list_fr=[]
        with open('перечень_слов_для_регресс.txt', 'r') as f2:
            file_word=f2.read().split(' ')#Слова с корреляцией
            for i in range(len(word_fr)):
                list_word_fr=(word_fr[i]).split(' ')
                if list_word_fr[0]!='':
                    list_word.append(list_word_fr[0])
                    list_fr.append(list_word_fr[1])
            for word in file_word:
                if word in list_word:
                    data[0].append(float(list_fr[list_word.index(word)]))
                else:
                    data[0].append(float(0))
        predict_clf_active = model_Active.predict(data)
        predict_clf_control=model_Control.predict(data)
        predict_clf_depress=model_Depress.predict(data)

        predict_clf_dover=model_Dover.predict(data)
        predict_clf_dream=model_Dream.predict(data)
        predict_clf_labil=model_Labil.predict(data)
        predict_clf_napr=model_Napr.predict(data)

        predict_clf_obshe=model_Obshe.predict(data)


        portret=[int(predict_clf_active[0]),int(predict_clf_control[0]),int(predict_clf_obshe[0]),int(predict_clf_napr[0]),int(predict_clf_depress[0]),int(predict_clf_dover[0]),int(predict_clf_dream[0]),int(predict_clf_labil[0])]
        probability=pr.proximity([[10,7,5,10,13,7,10,12]],[0.125,0.125,0.125,0.125,0.125,0.125,0.125,0.125],portret)

        predict_clf='активность {} \n' \
                    'самоконтроль {} \n' \
                    'общительность {} \n' \
                    'напряженность {} \n' \
                    'деприсивность {} \n' \
                    'доверчивость {} \n' \
                    'мечтательность {} \n' \
                    'эмоциональная лабильность {}'.format(portret[0],portret[1],portret[2],portret[3],portret[4],portret[5],portret[6],portret[7])


        return (predict_clf)


